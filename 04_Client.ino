
void InitClientWiFi()
{
  Serial.print("connecting to ");
  Serial.print(host);
  Serial.print(':');
  Serial.println(port);
  
  if (!client.connect(host, port)) {
    Serial.println("La conexión con el servidor falló");
    delay(2000);
    return;
  }
}

void SendDataToServer(String data)
{
  Serial.println("Enviando información al servidor");
  if (client.connected()) 
  {
    client.print(data);
  }
}
