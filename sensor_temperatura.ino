#include "Arduino.h"
#include <Ethernet.h>
#include <ESP8266WiFi.h>

#include "config.h"
#include "ESP8266_Utils.hpp"
#include "Client.hpp"

//#include "ESP8266_Utils_Client.hpp"
/**********************************************************************/
WiFiClient client;
void setup()
{
  
	Serial.begin(112560);
	dht12.begin();
  ConnectWiFi_STA();
  InitClientWiFi();
}

void loop()
{
  if (WiFi.status() != WL_CONNECTED)
  {
    ConnectWiFi_STA();
  }
  if (!client.connected())
    InitClientWiFi();
   else
   {
    String data = ReadSensorData();
    Serial.println(data);
    SendDataToServer(data);
   }
    
  
  delay(5000);
	
  
}
