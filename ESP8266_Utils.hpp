void ConnectWiFi_STA(bool useStaticIP = false)
{
   Serial.println("");
   WiFi.mode(WIFI_STA);
   WiFi.begin(ssid, password);
   if(useStaticIP) WiFi.config(ip, gateway, subnet);
   while (WiFi.status() != WL_CONNECTED) 
   { 
     delay(100);  
     Serial.print('.'); 
   }
 
   Serial.println("");
   Serial.print("Iniciado STA:\t");
   Serial.println(ssid);
   Serial.print("IP address:\t");
   Serial.println(WiFi.localIP());
}

void ConnectWiFi_AP(bool useStaticIP = false)
{ 
   Serial.println("");
   WiFi.mode(WIFI_AP);
   while(!WiFi.softAP(ssid, password))
   {
     Serial.println(".");
     delay(100);
   }
   if(useStaticIP) WiFi.softAPConfig(ip, gateway, subnet);

   Serial.println("");
   Serial.print("Iniciado AP:\t");
   Serial.println(ssid);
   Serial.print("IP address:\t");
   Serial.println(WiFi.softAPIP());
}

int cnt; 
    int flag = false; 
    int maxcount = 30; 
//    int cnt = 0;

void ConnectWiFi_WPS() 
{ 
  flag = false; 
  cnt = maxcount; 

  while (WiFi.status() != WL_CONNECTED) 
  { 
    delay(200); 
    if (WiFi.status() == WL_CONNECTED) break; 
    
    Serial.print("."); 
    if (cnt++ >= maxcount) 
    { 
      Serial.println("\nAttempting connection ..."); 
      Serial.println(""); 
      WiFi.beginWPSConfig(); 
      cnt = 0; 
      while (WiFi.status() != WL_CONNECTED) 
      { 
         Serial.print("*"); 
         delay(200); 
         if (cnt++ >= (maxcount >> 1)) break; 
      } 
      cnt = maxcount >> 1; 
    } 
  } 
  if (flag) return; 
  flag = true; 
  Serial.println("\r\nConnected!"); 
  Serial.println(WiFi.localIP()); 
  Serial.println(WiFi.SSID()); 
  Serial.println(WiFi.macAddress()
); 
    } 
