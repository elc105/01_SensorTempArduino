#include <ESP8266WiFi.h>

class WPS { 
  private: 
    int cnt; 
    int _cflag = false; 
    int _maxcount = 30; 
    int _BUTTON = 4; 
    int _LED = 12; 
  public: 
    void handleClient() { 
      cnt = 0; 
      if ((digitalRead(_BUTTON)==LOW)) { 
        while ((digitalRead(_BUTTON)==LOW)) delay(100); 
        _cflag = false; 
        cnt = _maxcount; 
        WiFi.disconnect(); 
        delay(100); 
      } 
      while (WiFi.status() != WL_CONNECTED) { 
        delay(200); 
        if (WiFi.status() == WL_CONNECTED) break; 
        Serial.print("."); 
        if (cnt++ >= _maxcount) { 
          //digitalWrite(_LED, HIGH); 
          Serial.println("\nAttempting connection ..."); 
          WiFi.beginWPSConfig(); 
          cnt = 0; 
          while (WiFi.status() != WL_CONNECTED) { 
            Serial.print("*"); 
            delay(200); 
            if (cnt++ >= (_maxcount >> 1)) break; 
          } 
          cnt = _maxcount >> 1; 
          //digitalWrite(_LED, LOW); 
        } 
      } 
      if (_cflag) return; 
      _cflag = true; 
      digitalWrite(_LED, LOW); 
      Serial.println("\r\nConnected!"); 
      Serial.println(WiFi.localIP()); 
      Serial.println(WiFi.SSID()); 
      Serial.println(WiFi.macAddress()); 
    } 
    /*
    WPS(int BUTTON, int LED) { 
      _BUTTON = BUTTON; 
      _LED = LED; 
      pinMode(_BUTTON, INPUT_PULLUP); 
      pinMode(_LED, OUTPUT); 
    } */
    WPS(){    }
}; 
